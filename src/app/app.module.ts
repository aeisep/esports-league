import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// PrimeNG
import { InputTextModule } from 'primeng/inputtext';
import { PasswordModule } from 'primeng/password';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { ToastModule } from 'primeng/toast';
import { TabMenuModule } from 'primeng/tabmenu';
import { TableModule } from 'primeng/table';
import {AccordionModule} from 'primeng/accordion';
import {TimelineModule} from 'primeng/timeline';
import { ListboxModule } from 'primeng/listbox';
import { TooltipModule } from 'primeng/tooltip';
import { CheckboxModule } from 'primeng/checkbox';
import { DialogModule } from 'primeng/dialog';

import { MessageService } from 'primeng/api';

// Components
import { HomePageComponent } from './home-page/home-page.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { FooterComponent } from './footer/footer.component';
import { AdminComponent } from './admin/admin.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { StandingsComponent } from './standings/standings.component';
import { StreamComponent } from './stream/stream.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { SignInComponent } from './sign-in/sign-in.component';

// Admin Tabs
import { CreateEditionComponent } from './admin/create-edition/create-edition.component';
import { UpdateEditionComponent } from './admin/update-edition/update-edition.component';
import { CreateGameComponent } from './admin/create-game/create-game.component';
import { UpdateGameComponent } from './admin/update-game/update-game.component';
import { CreateTeamComponent } from './admin/create-team/create-team.component';
import { CreatePlayerComponent } from './admin/create-player/create-player.component';
import { CreateMatchComponent } from './admin/create-match/create-match.component';
import { UpdateMatchComponent } from './admin/update-match/update-match.component';
import { RescheduleMatchComponent } from './admin/reschedule-match/reschedule-match.component';
import { TeamsComponent } from './teams/teams.component';
import { PlayersComponent } from './players/players.component';

import { ErrorInterceptor } from './authentication/interceptors/error.interceptor';
import { TokenInterceptor } from './authentication/interceptors/token.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    SideBarComponent,
    FooterComponent,
    AdminComponent,
    ScheduleComponent,
    StandingsComponent,
    StreamComponent,
    StatisticsComponent,
    SignInComponent,

    // admin tabs
    CreateEditionComponent,
    UpdateEditionComponent,
    CreateGameComponent,
    UpdateGameComponent,
    CreateTeamComponent,
    CreatePlayerComponent,
    CreateMatchComponent,
    UpdateMatchComponent,
    RescheduleMatchComponent,
    TeamsComponent,
    PlayersComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,

    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,

    // PrimeNG
    InputTextModule,
    AccordionModule,
    PasswordModule,
    DropdownModule,
    ButtonModule,
    ToastModule,
    TabMenuModule,
    TableModule,
    TimelineModule,
    ListboxModule,
    TooltipModule,
    CheckboxModule,
    DialogModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
