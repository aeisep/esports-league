import { Component, OnInit } from '@angular/core';
import Parallax from 'parallax-js';
import { ConfigService } from './config/config.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private configService: ConfigService) { }

  ngOnInit(): void {
    const scene = document.getElementById('background')!;
    const parallax = new Parallax(scene);

    // try to avoid slow api
    this.configService.fetchStatus();
  }
}
