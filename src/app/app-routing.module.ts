import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Admin tabs
import { AdminComponent } from './admin/admin.component';
import { CreateEditionComponent } from './admin/create-edition/create-edition.component';
import { CreateGameComponent } from './admin/create-game/create-game.component';
import { CreateMatchComponent } from './admin/create-match/create-match.component';
import { CreatePlayerComponent } from './admin/create-player/create-player.component';
import { CreateTeamComponent } from './admin/create-team/create-team.component';
import { RescheduleMatchComponent } from './admin/reschedule-match/reschedule-match.component';
import { UpdateEditionComponent } from './admin/update-edition/update-edition.component';
import { UpdateGameComponent } from './admin/update-game/update-game.component';
import { UpdateMatchComponent } from './admin/update-match/update-match.component';
import { AuthGuard } from './authentication/guards/auth.guard';
import { SignUpGuard } from './authentication/guards/sign-up.guard';

import { HomePageComponent } from './home-page/home-page.component';
import { PlayersComponent } from './players/players.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { StandingsComponent } from './standings/standings.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { StreamComponent } from './stream/stream.component';
import { TeamsComponent } from './teams/teams.component';

const routes: Routes = [
  {
    path: 'admin', component: AdminComponent, children: [
      { path: 'create-edition', component: CreateEditionComponent },
      { path: 'update-edition', component: UpdateEditionComponent },
      { path: 'create-game', component: CreateGameComponent },
      { path: 'update-game', component: UpdateGameComponent },
      { path: 'create-team', component: CreateTeamComponent },
      { path: 'create-player', component: CreatePlayerComponent },
      { path: 'create-match', component: CreateMatchComponent },
      { path: 'update-match', component: UpdateMatchComponent },
      { path: 'reschedule-match', component: RescheduleMatchComponent },
      { path: '', redirectTo: 'create-edition', pathMatch: 'full' }
    ],
    canActivate: [AuthGuard]
  },
  { path: 'login', component: SignInComponent },
  { path: 'classificacao', component: StandingsComponent, canActivate: [SignUpGuard] },
  { path: 'estatisticas', component: StatisticsComponent, canActivate: [SignUpGuard] },
  { path: 'equipas', component: TeamsComponent, canActivate: [SignUpGuard] },
  { path: 'horario', component: ScheduleComponent, canActivate: [SignUpGuard] },
  { path: 'jogadores', component: PlayersComponent, canActivate: [SignUpGuard] },
  { path: 'transmissao', component: StreamComponent, canActivate: [SignUpGuard] },
  { path: '', component: HomePageComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
