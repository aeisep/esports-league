import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StandingsService {

  constructor(private http: HttpClient) { }

  public fetchStandings(): Observable<any> {
    return this.http.get<any>('assets/statistics_dummy.json');
  }
}
