import { Component, OnInit } from '@angular/core';

import { MenuItem } from 'primeng/api';

import { StandingsService } from './service/standings.service';

@Component({
  selector: 'app-standings',
  templateUrl: './standings.component.html',
  styleUrls: ['./standings.component.scss']
})
export class StandingsComponent implements OnInit {

  public isLoading: boolean = true;

  public groups: MenuItem[] = [
    { label: 'Grupo A' },
    { label: 'Grupo B' },
    { label: 'Grupo C' },
    { label: 'Grupo D' }
  ]

  public columns: any[] = [
    { header: '#', field: 'key', width: '3%', canSort: false },
    { header: 'Nome', field: 'name', width: '10%', canSort: true },
    { header: 'Equipa', field: 'team', width: '10%', canSort: true },
    { header: 'KDR (Kill/Death Ratio)', field: 'kd', width: '11%', canSort: true },
    { header: 'Kills', field: 'kills', width: '6%', canSort: true },
    { header: 'Deaths', field: 'deaths', width: '8%', canSort: true },
    { header: 'Assists', field: 'assists', width: '7%', canSort: true },
    { header: 'Headshots', field: 'headshots', width: '9%', canSort: true },
    { header: 'HS %', field: 'hspercent', width: '15%', canSort: true },
    { header: 'Win %', field: 'winpercent', width: '15%', canSort: true },
    { header: 'W-L', field: 'wl', width: '6%', canSort: true },
  ];

  public dataSource: any[];

  constructor(private service: StandingsService) { }

  ngOnInit(): void {
    /* temp */
    setTimeout(() => this.service.fetchStandings().subscribe((statistics: any) => {
      this.dataSource = statistics.items.map((value: any, index: any) => ({ key: index + 1, ...value, wl: `${value.wins} - ${value.losses}` }));
      this.isLoading = false;
    }), 2000);
  }

}
