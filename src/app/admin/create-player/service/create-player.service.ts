import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CreatePlayerService {
  private route: string = `${environment.api}/api/players`;
  private teamRoute: string = `${environment.api}/api/teams`;

  constructor(private http: HttpClient) { }

  public fetchActivePlayers(): Observable<any> {
    return this.http.get(`${this.route}/active`);
  }

  public fetchPlayersByTeam(teamId: string): Observable<any> {
    return this.http.get(`${this.route}/${teamId}`);
  }

  public createPlayer(players: any[], editionId: number, gameId: number, name: string): Observable<any> {
    return this.http.post(`${this.teamRoute}/singlePlayer`, { players: players, editionId: editionId, gameId: gameId, name: name });
  }
}
