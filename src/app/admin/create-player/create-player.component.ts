import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { CreateEditionService } from '../create-edition/service/create-edition.service';
import { CreateGameService } from '../create-game/service/create-game.service';
import { CreateTeamService } from '../create-team/service/create-team.service';
import { CreatePlayerService } from './service/create-player.service';

@Component({
  selector: 'app-create-player',
  templateUrl: './create-player.component.html',
  styleUrls: ['./create-player.component.scss']
})
export class CreatePlayerComponent implements OnInit {

  public displayHelp: boolean = false;

  public activeEdition: { id: number, name: string, status: number } = null;
  public games: any[];
  public teams: any[];
  public degrees: any[];

  public players: any[] = [];
  public visiblePlayers: any[];

  public createPlayerForm: FormGroup = new FormGroup({
    game: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    nickname: new FormControl('', Validators.required),
    degree: new FormControl('', Validators.required)
  });

  constructor(
    private service: CreatePlayerService,
    private editionService: CreateEditionService,
    private gameService: CreateGameService,
    private teamService: CreateTeamService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.fetchOngoingEdition();
    // yikes!
    this.fetchGames().then(
      () => this.fetchTeams().then(
        () => this.fetchPlayers(),
        () => console.error("ERROR!")
      ),
      () => console.error("ERROR!")
    );
    this.setupDegrees();
  }

  public showDialog(): void {
    this.displayHelp = true;
  }

  private fetchOngoingEdition(): void {
    this.editionService.fetchOngoingEdition().subscribe((response) => {
      console.log(`Server response [ONGOING EDITION]: `, response);
      this.activeEdition = { id: response.id, name: response.name, status: response.status };
    },
      (error) => {
        console.warn(`Server error: `, error);
        this.messageService.add({ severity: 'error', summary: 'Erro', detail: 'Edição ativa não encontrada!' });
      });
  }

  private fetchGames(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.gameService.fetchActiveGames().subscribe((response) => {
        console.log(`Server response [ACTIVE GAMES]: `, response);
        this.games = response
          .filter((game: any) => game.isSingleplayer)
          .map((game: any) => ({ id: game.id, name: game.name, status: game.status }));
        resolve();
      },
        (error) => {
          console.warn(`Server error: `, error);
          this.games = [];
          this.messageService.add({ severity: 'error', summary: 'Erro', detail: 'Jogos não encontrados!' });
          reject();
        });
    });
  }

  private fetchTeams(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.teamService.fetchTeams().subscribe((response) => {
        console.log(`Server response [TEAMS]: `, response);
        console.log(this.games);
        this.teams = response
          .filter((team: any) => this.games.some((game) => game.id === team.gameId))
          .map((team: any) => ({ id: team.id, game: this.games.find(game => game.id === team.gameId).name }))
        resolve();
      },
        (error) => {
          console.warn(`Server error: `, error);
          this.teams = [];
          this.messageService.add({ severity: 'error', summary: 'Erro', detail: 'Equipas não encontradas!' });
          reject();
        });
    });
  }

  private fetchPlayers(): void {
    this.service.fetchActivePlayers().subscribe((response) => {
      console.log(`Server response [PLAYERS]: `, response);
      this.players = response.map((player: any) => ({ id: player.id, name: player.name, nickname: player.nickname, game: this.teams.find(team => team.id === player.teamId).game }));
      this.visiblePlayers = this.players;
    },
      (error) => {
        console.warn(`Server error: `, error);
        this.players = [];
        this.messageService.add({ severity: 'error', summary: 'Erro', detail: 'Jogadores não encontrados!' });
      })
  }

  public filterPlayers(): void {
    console.log("Filtering players...");
    console.log(this.createPlayerForm.value.game);
    const gameName = this.games.find((game) => game.id === this.createPlayerForm.value.game).name;
    this.visiblePlayers = this.players.filter((player) => player.game === gameName);
  }

  private setupDegrees(): void {
    this.degrees = [
      { label: "Biorrecursos", value: "Biorrecursos" },
      { label: "Engenharia Biomédica", value: "Engenharia Biomédica" },
      { label: "Engenharia Civil", value: "Engenharia Civil" },
      { label: "Engenharia de Telecomunicações e Informática", value: "Engenharia de Telecomunicações e Informática" },
      { label: "Engenharia Eletrotécnica e Computadores", value: "Engenharia Eletrotécnica e Computadores" },
      { label: "Engenharia Eletrotécnica e Computadores", value: "Engenharia Eletrotécnica e Computadores" },
      { label: "Engenharia e Gestão Industrial", value: "Engenharia e Gestão Industrial" },
      { label: "Engenharia Geotécnica e Geoambiente", value: "Engenharia Geotécnica e Geoambiente" },
      { label: "Engenharia Informática", value: "Engenharia Informática" },
      { label: "Engenharia Mecânica", value: "Engenharia Mecânica" },
      { label: "Engenharia Mecânica Automóvel", value: "Engenharia Mecânica Automóvel" },
      { label: "Engenharia Química", value: "Engenharia Química" },
      { label: "Engenharia de Sistemas", value: "Engenharia de Sistemas" },]
  }

  public createPlayer() {
    const values = this.createPlayerForm.value;
    this.service.createPlayer([{ name: values.name, nickname: values.nickname, degree: values.degree, image_url: '' }],
      this.activeEdition.id, values.game, values.name).subscribe((response) => {
        console.log(`Server response: `, response);
        this.fetchTeams().then(
          () => this.fetchPlayers(),
          () => console.error("ERROR!")
        );
        this.createPlayerForm.reset();
        this.messageService.add({ severity: 'success', summary: 'Informação', detail: 'Jogador criado com sucesso!' });
      },
        (error) => {
          console.warn(`Server error: `, error);
          this.messageService.add({ severity: 'error', summary: 'Erro', detail: 'Ocorreu um erro ao criar o jogador!' });
        });
  }

}
