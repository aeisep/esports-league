import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-match',
  templateUrl: './create-match.component.html',
  styleUrls: ['./create-match.component.scss']
})
export class CreateMatchComponent implements OnInit {

  createMatchForm: FormGroup = new FormGroup({
    player1: new FormControl('', Validators.required)
  });

  constructor() { }

  ngOnInit(): void {
  }

  createMatch() {
    console.log("Creating Match...");
  }

}
