import { TestBed } from '@angular/core/testing';

import { CreateMatchService } from './create-match.service';

describe('CreateMatchService', () => {
  let service: CreateMatchService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CreateMatchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
