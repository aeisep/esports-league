import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { MessageService } from 'primeng/api';

import { CreateEditionService } from './service/create-edition.service';

@Component({
  selector: 'app-create-edition',
  templateUrl: './create-edition.component.html',
  styleUrls: ['./create-edition.component.scss']
})
export class CreateEditionComponent implements OnInit {

  public displayHelp: boolean = false;
  public editions: any[];

  public createEditionForm: FormGroup = new FormGroup({
    name: new FormControl('', Validators.required)
  });

  constructor(
    private service: CreateEditionService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.fetchEditions();
  }

  public showDialog(): void {
    this.displayHelp = true;
  }

  private fetchEditions(): void {
    this.service.fetchEditions().subscribe((response) => {
      console.log(`Server response: `, response);
      this.editions = response.map((edition: any) => ({ name: edition.name, status: edition.status }));
    },
      (error) => {
        console.warn(`Server error: `, error);
        this.editions = [];
        this.messageService.add({ severity: 'error', summary: 'Erro', detail: 'Edições não encontradas!' });
      });
  }

  public createEdition(): void {
    const values = this.createEditionForm.value;
    this.service.createEdition(values.name).subscribe((response) => {
      console.log(`Server response: `, response);
      this.fetchEditions();
      this.createEditionForm.reset();
      this.messageService.add({ severity: 'success', summary: 'Informação', detail: 'Edição criada com sucesso!' });
    },
      (error) => {
        console.warn(`Server error: `, error);
        this.messageService.add({ severity: 'error', summary: 'Erro', detail: 'Ocorreu um erro ao criar a edição!' });
      });
  }
}
