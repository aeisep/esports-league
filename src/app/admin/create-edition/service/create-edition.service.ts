import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CreateEditionService {
  private route: string = `${environment.api}/api/editions`

  constructor(private http: HttpClient) { }

  public fetchEditions(): Observable<any> {
    return this.http.get(this.route);
  }

  public fetchOngoingEdition(): Observable<any> {
    return this.http.get(`${this.route}/ongoing`);
  }

  public createEdition(name: string): Observable<any> {
    return this.http.post(this.route, { name: name });
  }
}
