import { TestBed } from '@angular/core/testing';

import { CreateEditionService } from './create-edition.service';

describe('CreateEditionService', () => {
  let service: CreateEditionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CreateEditionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
