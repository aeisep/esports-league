import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { MessageService } from 'primeng/api';

import { CreateGameService } from './service/create-game.service';

@Component({
  selector: 'app-create-game',
  templateUrl: './create-game.component.html',
  styleUrls: ['./create-game.component.scss']
})
export class CreateGameComponent implements OnInit {

  public displayHelp: boolean = false;
  public games: any[];

  public createGameForm: FormGroup = new FormGroup({
    name: new FormControl('', Validators.required),
    isSingleplayer: new FormControl(false, Validators.required)
  });

  constructor(
    private service: CreateGameService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.fetchGames();
  }

  public showDialog(): void {
    this.displayHelp = true;
  }

  private fetchGames(): void {
    this.service.fetchGames().subscribe((response) => {
      console.log(`Server response: `, response);
      this.games = response.map((game: any) => ({ name: game.name, status: game.status, isSingleplayer: game.isSingleplayer }));
    },
      (error) => {
        console.warn(`Server error: `, error);
        this.games = [];
        this.messageService.add({ severity: 'error', summary: 'Erro', detail: 'Jogos não encontrados!' });
      });
  }

  public createGame(): void {
    const values = this.createGameForm.value;
    this.service.createGame(values.name, values.isSingleplayer).subscribe((response) => {
      console.log(`Server response: `, response);
      this.fetchGames();
      this.createGameForm.reset();
      this.messageService.add({ severity: 'success', summary: 'Informação', detail: 'Jogo criado com sucesso!' });
    },
      (error) => {
        console.warn(`Server error: `, error);
        this.messageService.add({ severity: 'error', summary: 'Erro', detail: 'Ocorreu um erro ao criar o jogo!' });
      });
  }
}
