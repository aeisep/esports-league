import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CreateGameService {
  private route: string = `${environment.api}/api/games`

  constructor(private http: HttpClient) { }

  public fetchGames(): Observable<any> {
    return this.http.get(this.route);
  }

  public fetchActiveGames(): Observable<any> {
    return this.http.get(`${this.route}/active`);
  }

  public createGame(name: string, isSingleplayer: boolean): Observable<any> {
    return this.http.post(this.route, { name: name, isSingleplayer: isSingleplayer });
  }
}
