import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-reschedule-match',
  templateUrl: './reschedule-match.component.html',
  styleUrls: ['./reschedule-match.component.scss']
})
export class RescheduleMatchComponent implements OnInit {

  rescheduleMatchForm: FormGroup = new FormGroup({
    game: new FormControl('', Validators.required)
  });

  constructor() { }

  ngOnInit(): void {
  }

  rescheduleMatch() {
    console.log("Rescheduling match...");
  }

}
