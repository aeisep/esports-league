import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RescheduleMatchComponent } from './reschedule-match.component';

describe('RescheduleMatchComponent', () => {
  let component: RescheduleMatchComponent;
  let fixture: ComponentFixture<RescheduleMatchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RescheduleMatchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RescheduleMatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
