import { TestBed } from '@angular/core/testing';

import { RescheduleMatchService } from './reschedule-match.service';

describe('RescheduleMatchService', () => {
  let service: RescheduleMatchService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RescheduleMatchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
