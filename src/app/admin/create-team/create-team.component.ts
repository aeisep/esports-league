import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { MessageService } from 'primeng/api';
import { CreateEditionService } from '../create-edition/service/create-edition.service';
import { CreateGameService } from '../create-game/service/create-game.service';
import { CreatePlayerService } from '../create-player/service/create-player.service';

import { CreateTeamService } from './service/create-team.service';

@Component({
  selector: 'app-create-team',
  templateUrl: './create-team.component.html',
  styleUrls: ['./create-team.component.scss']
})
export class CreateTeamComponent implements OnInit {

  public displayHelp: boolean = false;

  public activeEdition: { id: number, name: string, status: number } = null;
  public games: any[];
  public teams: any[];
  public visibleTeams: any[];
  public degrees: any[];

  public selectedPlayer: any;
  public players: any[] = [];

  public teamTooltip: string = "A carregar...";

  public createTeamForm: FormGroup = new FormGroup({
    game: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required)
  });

  public addPlayerForm: FormGroup = new FormGroup({
    name: new FormControl('', Validators.required),
    nickname: new FormControl('', Validators.required),
    degree: new FormControl('', Validators.required)
  });

  constructor(
    private service: CreateTeamService,
    private editionService: CreateEditionService,
    private gameService: CreateGameService,
    private playerService: CreatePlayerService,
    private messageService: MessageService,
  ) { }

  ngOnInit(): void {
    this.fetchOngoingEdition();
    this.fetchGames().then(
      () => this.fetchTeams(),
      () => console.error("ERROR!")
    );
    this.setupDegrees();
  }

  public showDialog(): void {
    this.displayHelp = true;
  }

  private fetchOngoingEdition(): void {
    this.editionService.fetchOngoingEdition().subscribe((response) => {
      console.log(`Server response [ONGOING EDITION]: `, response);
      this.activeEdition = { id: response.id, name: response.name, status: response.status };
    },
      (error) => {
        console.warn(`Server error: `, error);
        this.messageService.add({ severity: 'error', summary: 'Erro', detail: 'Edição ativa não encontrada!' });
      });
  }

  private fetchGames(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.gameService.fetchActiveGames().subscribe((response) => {
        console.log(`Server response [ACTIVE GAMES]: `, response);
        this.games = response
          .filter((game: any) => !game.isSingleplayer)
          .map((game: any) => ({ id: game.id, name: game.name, status: game.status }));
        resolve();
      },
        (error) => {
          console.warn(`Server error: `, error);
          this.games = [];
          this.messageService.add({ severity: 'error', summary: 'Erro', detail: 'Jogos não encontrados!' });
          reject();
        });
    })
  }

  private fetchTeams(): void {
    this.service.fetchTeams().subscribe((response) => {
      console.log(`Server response [TEAMS]: `, response);
      this.teams = response
        .filter((team: any) => this.games.some((game) => game.id === team.gameId))
        .map((team: any) => ({ id: team.id, name: team.name, game: this.games.find(game => game.id === team.gameId).name }))
        .sort((team1: any, team2: any) => team1.game.localeCompare(team2.game));
      this.visibleTeams = this.teams;
    },
      (error) => {
        console.warn(`Server error: `, error);
        this.teams = [];
        this.messageService.add({ severity: 'error', summary: 'Erro', detail: 'Equipas não encontradas!' });
      });
  }

  public fetchPlayersByTeam(id: string): void {
    this.playerService.fetchPlayersByTeam(id).subscribe((response) => {
      console.log(`Server response [PLAYERS BY TEAM]: `, response);
      this.teamTooltip = response.reduce((acc: any, cur: any) => {
        return acc + `${cur.name} | ${cur.nickname}\n`;
      }, '');
    },
      (error) => {
        console.warn(`Server error [PLAYERS BY TEAM]: `, error);
        this.teamTooltip = "Erro!";
      });
  }

  public filterTeams(): void {
    const gameName = this.games.find((game) => game.id === this.createTeamForm.value.game).name;
    this.visibleTeams = this.teams.filter((team) => team.game === gameName);
  }

  public resetTooltip(): void {
    this.teamTooltip = "A carregar...";
  }

  private setupDegrees(): void {
    this.degrees = [
      { label: "Biorrecursos", value: "Biorrecursos" },
      { label: "Engenharia Biomédica", value: "Engenharia Biomédica" },
      { label: "Engenharia Civil", value: "Engenharia Civil" },
      { label: "Engenharia de Telecomunicações e Informática", value: "Engenharia de Telecomunicações e Informática" },
      { label: "Engenharia Eletrotécnica e Computadores", value: "Engenharia Eletrotécnica e Computadores" },
      { label: "Engenharia Eletrotécnica e Computadores", value: "Engenharia Eletrotécnica e Computadores" },
      { label: "Engenharia e Gestão Industrial", value: "Engenharia e Gestão Industrial" },
      { label: "Engenharia Geotécnica e Geoambiente", value: "Engenharia Geotécnica e Geoambiente" },
      { label: "Engenharia Informática", value: "Engenharia Informática" },
      { label: "Engenharia Mecânica", value: "Engenharia Mecânica" },
      { label: "Engenharia Mecânica Automóvel", value: "Engenharia Mecânica Automóvel" },
      { label: "Engenharia Química", value: "Engenharia Química" },
      { label: "Engenharia de Sistemas", value: "Engenharia de Sistemas" },]
  }

  private resetPlayers() {
    this.players = [];
  }

  public removePlayer(): void {
    this.players.splice(this.players.indexOf(this.selectedPlayer), 1);
    delete this.selectedPlayer;
  }

  public addPlayerToTeam(): void {
    if (this.players.length === 7) {
      this.messageService.add({ severity: 'error', summary: 'Erro', detail: 'Máximo número de jogadores por equipa!' });
      return;
    }
    const player = this.addPlayerForm.value;
    this.players = [...this.players, { name: player.name, nickname: player.nickname, degree: player.degree, image_url: '' }];
    this.addPlayerForm.reset();
  }

  public createTeam(): void {
    const values = this.createTeamForm.value;
    this.service.createTeam(this.players, this.activeEdition.id, values.game, values.name).subscribe((response) => {
      console.log(`Server response: `, response);
      this.fetchTeams();
      this.resetPlayers();
      this.addPlayerForm.reset();
      this.createTeamForm.reset();
      this.messageService.add({ severity: 'success', summary: 'Informação', detail: 'Equipa criada com sucesso!' });
    },
      (error) => {
        console.warn(`Server error: `, error);
        this.messageService.add({ severity: 'error', summary: 'Erro', detail: 'Ocorreu um erro ao criar a equipa!' });
      });
  }
}
