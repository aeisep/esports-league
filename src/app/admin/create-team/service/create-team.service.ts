import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CreateTeamService {
  private route: string = `${environment.api}/api/teams`

  constructor(private http: HttpClient) { }

  public fetchTeams(): Observable<any> {
    return this.http.get(`${this.route}/active`);
  }

  public createTeam(players: any[], editionId: number, gameId: number, name: string): Observable<any> {
    return this.http.post(this.route, { players: players, editionId: editionId, gameId: gameId, name: name });
  }
}
