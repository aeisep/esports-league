import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-update-match',
  templateUrl: './update-match.component.html',
  styleUrls: ['./update-match.component.scss']
})
export class UpdateMatchComponent implements OnInit {

  updateMatchForm: FormGroup = new FormGroup({
    match: new FormControl('', Validators.required)
  });

  constructor() { }

  ngOnInit(): void {
  }

  updateMatch() {
    console.log("Updating match...");
  }

}
