import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  tabs: MenuItem[] = [
    { label: 'Adicionar Edição', icon: "fas fa-folder-plus", routerLink: ['create-edition'] },
    { label: 'Alterar Edição', icon: "fas fa-folder-open", routerLink: ['update-edition'] },
    { label: 'Adicionar Jogo', icon: "fas fa-gamepad", routerLink: ['create-game'] },
    { label: 'Alterar Jogo', icon: "fas fa-toggle-off", routerLink: ['update-game'] },
    { label: 'Adicionar Equipa', icon: "fas fa-shield-virus", routerLink: ['create-team'] },
    { label: 'Adicionar Jogador', icon: "fas fa-user-astronaut", routerLink: ['create-player'] },
    { label: 'Adicionar Partida', icon: "fas fa-calendar-plus", routerLink: ['create-match'] },
    { label: 'Atualizar Partida', icon: "fas fa-calendar-check", routerLink: ['update-match'] },
    { label: 'Remarcar Partida', icon: "fas fa-calendar-times", routerLink: ['reschedule-match'] },
  ]

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private messageService: MessageService) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['session']) {
        setTimeout(() => this.messageService.add({ severity: 'success', summary: 'Informação', detail: 'Sessão restaurada!' }), 10);
      }
    })
  }

  // auxiliary method to display correct active tab if user routes by url directly
  findActiveRoute(): MenuItem {
    return this.tabs.find((tab) => tab.routerLink[0] === this.router.url.split('/')[2]) || this.tabs[0];
  }
}
