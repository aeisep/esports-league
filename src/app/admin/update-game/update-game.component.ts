import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { UpdateGameService } from './service/update-game.service';

@Component({
  selector: 'app-update-game',
  templateUrl: './update-game.component.html',
  styleUrls: ['./update-game.component.scss']
})
export class UpdateGameComponent implements OnInit {

  public displayHelp: boolean = false;
  public games: any[];

  public status: any[] = [
    { number: 0, description: 'Inativo' },
    { number: 1, description: 'Ativo' }
  ];

  public updateGameForm: FormGroup = new FormGroup({
    game: new FormControl('', Validators.required),
    status: new FormControl('', Validators.required)
  });

  constructor(
    private service: UpdateGameService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.fetchGames();
  }

  public showDialog(): void {
    this.displayHelp = true;
  }

  private fetchGames(): void {
    this.service.fetchGames().subscribe((response) => {
      console.log(`Server response: `, response);
      this.games = response.map((game: any) => ({ id: game.id, name: game.name, status: game.status, isSingleplayer: game.isSingleplayer }));
    },
      (error) => {
        console.warn(`Server error: `, error);
        this.games = [];
        this.messageService.add({ severity: 'error', summary: 'Erro', detail: 'Jogos não encontrados!' });
      });
  }

  public updateGame(): void {
    const values = this.updateGameForm.value;
    this.service.patchGame(values.game, values.status).subscribe((response) => {
      console.log(`Server response: `, response);
      this.fetchGames();
      this.updateGameForm.reset();
      this.messageService.add({ severity: 'success', summary: 'Informação', detail: 'Jogo atualizado com sucesso!' });
    },
      (error) => {
        console.warn(`Server error: `, error);
        this.messageService.add({ severity: 'error', summary: 'Erro', detail: 'Ocorreu um erro ao atualizar o jogo!' });
      });
  }
}
