import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UpdateGameService {
  private route: string = `${environment.api}/api/games`

  constructor(private http: HttpClient) { }

  public fetchGames(): Observable<any> {
    return this.http.get(this.route);
  }

  public patchGame(id: string, status: number): Observable<any> {
    return this.http.patch(`${this.route}/${id}/${status}`, {});
  }
}
