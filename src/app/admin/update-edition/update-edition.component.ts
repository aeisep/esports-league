import {
  Component,
  OnInit
}

from '@angular/core';

import {
  FormControl,
  FormGroup,
  Validators
}

from '@angular/forms';

import {
  MessageService
}

from 'primeng/api';

import {
  UpdateEditionService
}

from './service/update-edition.service';

@Component( {
    selector: 'app-update-edition',
    templateUrl: './update-edition.component.html',
    styleUrls: ['./update-edition.component.scss']
  }

) export class UpdateEditionComponent implements OnInit {

  public displayHelp: boolean=false;
  public editions: any[];

  public status: any[]=[ {
    number: 0, description: 'Inativa'
  }

  ,
    {
    number: 1, description: 'Inscrições abertas'
  }

  ,
    {
    number: 2, description: 'A decorrer'
  }

  ];

  public updateEditionForm: FormGroup=new FormGroup( {
      edition: new FormControl('', Validators.required),
      status: new FormControl('', Validators.required)
    }

  );

  constructor(private service: UpdateEditionService,
    private messageService: MessageService) {}

  ngOnInit(): void {
    this.fetchEditions();
  }

  public showDialog(): void {
    this.displayHelp=true;
  }

  private fetchEditions(): void {
    this.service.fetchEditions().subscribe((response)=> {
        console.log(`Server response: `, response);

        this.editions=response.map((edition: any)=> ( {
              id: edition.id, name: edition.name, status: edition.status
            }

          ));
      }

      ,
      (error)=> {
        console.warn(`Server error: `, error);
        this.editions=[];

        this.messageService.add( {
            severity: 'error', summary: 'Erro', detail: 'Edições não encontradas!'
          }

        );
      }

    );
  }

  public updateEdition(): void {
    const values=this.updateEditionForm.value;

    this.service.patchEdition(values.edition, values.status).subscribe((response)=> {
        console.log(`Server response: `, response);
        this.fetchEditions();
        this.updateEditionForm.reset();

        this.messageService.add( {
            severity: 'success', summary: 'Informação', detail: 'Edição atualizada com sucesso!'
          }

        );
      }

      ,
      (error)=> {
        console.warn(`Server error: `, error);

        this.messageService.add( {
            severity: 'error', summary: 'Erro', detail: 'Ocorreu um erro ao atualizar a edição!'
          }

        );
      }

    );
  }

}