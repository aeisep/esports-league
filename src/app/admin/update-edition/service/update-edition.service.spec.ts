import { TestBed } from '@angular/core/testing';

import { UpdateEditionService } from './update-edition.service';

describe('UpdateEditionService', () => {
  let service: UpdateEditionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UpdateEditionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
