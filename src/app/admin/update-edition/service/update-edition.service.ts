import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UpdateEditionService {

  private route: string = `${environment.api}/api/editions`

  constructor(private http: HttpClient) { }

  public fetchEditions(): Observable<any> {
    return this.http.get(this.route);
  }

  public patchEdition(id: string, status: number): Observable<any> {
    return this.http.patch(`${this.route}/${id}/${status}`, {});
  }
}
