import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  constructor(private http: HttpClient) { }

  public fetchStatus(): void {
    this.http.head(`${environment.api}/status`)
      .toPromise()
      .then(() => console.log(`Server is ok and running!`))
      .catch(() => console.error(`Server is not running!`));
  }
}
