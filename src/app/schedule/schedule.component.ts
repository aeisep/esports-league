import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent implements OnInit {

  events: any[];

  constructor() { }

  ngOnInit(): void {
    this.events = [
      {status: 'JURUBURUS vs. GIGA NA DISNEY, 14/07 19:00', date: '15/10/2020 10:30', color: '#9C27B0'},
      {status: 'JURUBURUS vs. GIGA NA DISNEY, 14/07 19:00', date: '15/10/2020 14:00', color: '#673AB7'},
      {status: 'JURUBURUS vs. GIGA NA DISNEY, 14/07 19:00', date: '15/10/2020 16:15', color: '#FF9800'},
      {status: 'JURUBURUS vs. GIGA NA DISNEY, 14/07 19:00', date: '16/10/2020 10:00', color: '#607D8B'},
    ]
  }

}
