import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { MessageService } from 'primeng/api';

import { AuthenticationService } from '../authentication/service/authentication.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  showPwd: boolean = false;
  signInForm: FormGroup = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthenticationService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    // verify if tried to access admin before and unauthorized
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['authorized']) {
        setTimeout(() => this.messageService.add({ severity: 'error', summary: 'Erro', detail: 'Não tens autorização para ver essa página!' }), 10);
      }
    });

    // redirect to admin if already logged in
    if (this.authService.getCurrentUser()) {
      this.router.navigate(['/admin'], { queryParams: { session: 'open' } });
    }
  }

  public handleLogin(): void {
    const formValues = Object.assign({}, this.signInForm.value);

    this.authService.signIn(formValues.username, formValues.password).subscribe(result => {
      if (!result) {
        this.messageService.add({ severity: 'error', summary: 'Erro', detail: 'Username e/ou Password errado(s)!' });
        return;
      }
      this.router.navigate(['/admin']);
    });
  }
}
