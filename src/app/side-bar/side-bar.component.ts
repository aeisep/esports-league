import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {

  public isMenuOpen = false;

  public games: any[] = [
    {
      name: 'Counter-Strike: Global Offensive', shortName: 'CSGO', class: 'csgo', imageURL: '../../assets/img/csgo-icon.png',
      formLink: 'https://forms.gle/6wHz6TwLTR98cHF26', regulationFile: '../../assets/docs/Regulamento-CSGO.pdf', active: false
    },
    {
      name: 'League of Legends', shortName: 'LOL', class: 'lol', imageURL: '../../assets/img/lol-icon.webp',
      formLink: 'https://forms.gle/iCMsN3GHYCLXAgLZA', regulationFile: '../../assets/docs/Regulamento-LOL.pdf', active: false
    },
    {
      name: 'TFT', shortName: 'TFT', class: 'cr', imageURL: '../../assets/img/tft-icon.jpg',
      formLink: 'https://forms.gle/qXqDQBCKfs5he9G19 ', regulationFile: '../../assets/docs/Regulamento-TFT.pdf', active: false
    },
    {
      name: 'VALORANT', shortName: 'VALORANT', class: 'valorant', imageURL: '../../assets/img/valorant-icon.jpg',
      formLink: 'https://forms.gle/45P2h66GpWier8DA8', regulationFile: '../../assets/docs/Regulamento-VALORANT.pdf', active: false
    },
  ];


  public games2: any[] = [
    {
      name: 'Brawl Stars', shortName: 'BS', class: 'bs', imageURL: '../../assets/img/bs-icon.png',
      // tslint:disable-next-line:max-line-length
      formLink: 'https://docs.google.com/forms/d/e/1FAIpQLSeWM8N8BFYwhX6cxzz0qcfES6rVqm49T0bPgLuyte9i52urPw/viewform?usp=sf_link', regulationFile: '../../assets/docs/Regulamento-Brawl_Stars.pdf', active: false
    },
    {
      name: 'Clash Royale', shortName: 'CR', class: 'cr', imageURL: '../../assets/img/cr-icon.png',
      // tslint:disable-next-line:max-line-length
      formLink: 'https://docs.google.com/forms/d/e/1FAIpQLScILb1ZAvd1qbE_aEolBmmynIQv4-y3Pu-9Yrw1dXiHRqv7Yg/viewform?usp=sf_link', regulationFile: '../../assets/docs/Regulamento-Clash Royale.pdf', active: false
    },
    {
      name: 'FIFA 23', shortName: 'FIFA', class: 'fifa', imageURL: '../../assets/img/fifa23-icon.jpg',
      // tslint:disable-next-line:max-line-length
      formLink: 'https://forms.gle/pRnhkBimwmryzgfT9', regulationFile: '../../assets/docs/Regulamento-FIFA23.pdf', active: false
    },
  ];
  public selectedGame: any = null;

  constructor() { }

  ngOnInit(): void { }

  public selectGame(shortName: string, lista: string): void {
    if (lista === 'l1') {
      this.games.forEach((game) => {
        game.active = (game.shortName === shortName);
      });
      this.selectedGame = Object.assign({}, this.games.find((game) => (game.shortName === shortName)));
      this.games2.forEach((game) => {
        game.active = false;
      });
    }
    else {
      this.games2.forEach((game) => {
        game.active = (game.shortName === shortName);
      });
      this.selectedGame = Object.assign({}, this.games2.find((game) => (game.shortName === shortName)));
      this.games.forEach((game) => {
        game.active = false;
      });
    }

  }
}
