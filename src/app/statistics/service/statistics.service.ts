import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StatisticsService {

  constructor(private http: HttpClient) { }

  public fetchStatistics(): Observable<any> {
    return this.http.get<any>('assets/statistics_dummy.json');
  }
}
