export interface UserDTO {
    adminDTO: {
        username: string;
    };
    token: string;
}