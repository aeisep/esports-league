import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { AuthenticationService } from '../service/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthenticationService
  ) { }

  canActivate(): boolean {
    if (!this.authService.getCurrentUser()) {
      this.router.navigate(['/login'], { queryParams: { authorized: 'no' } });
      return false;
    }
    return true;
  }
}
