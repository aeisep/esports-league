import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import jwtDecode from 'jwt-decode';

import { environment } from 'src/environments/environment';
import { UserDTO } from '../dto/userDTO';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private currentUser: BehaviorSubject<User>;
  public currentUser$: Observable<User>;

  constructor(private http: HttpClient) {
    const user: User = JSON.parse(localStorage.getItem('user'));
    let currentUser: any;
    if ((!user) || ((Math.floor(Date.now() / 1000) + 7200) >= user.tokenExpiry)) {
      currentUser = null;
      localStorage.removeItem('user');
    } else {
      currentUser = user;
    }

    this.currentUser = new BehaviorSubject<User>(currentUser);
    this.currentUser$ = this.currentUser.asObservable();
  }

  public getCurrentUser(): User {
    return this.currentUser.getValue();
  }

  public signIn(username: string, password: string): Observable<boolean> {
    return this.http.post<UserDTO>(`${environment.api}/api/auth/signin`, { username: username, password: password }).pipe(
      map(userDTO => {
        if (!userDTO.token) return false;

        const decoded: any = jwtDecode(userDTO.token);

        const user: User = {
          username: userDTO.adminDTO.username,
          token: userDTO.token,
          tokenExpiry: decoded.exp,
        };

        localStorage.setItem('user', JSON.stringify(user));
        this.currentUser.next(user);
        return true;
      }),
      catchError(err => {
        console.warn(err);
        return of(false);
      }));
  }

  public signOut(): void {
    localStorage.removeItem('user');
    this.currentUser.next(null);
  }
}
