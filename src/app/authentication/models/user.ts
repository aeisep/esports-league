export interface User {
    username: string;
    token: string;
    tokenExpiry: number;
}