export const environment = {
  production: true,
  api: 'https://esports-isep-league-api.herokuapp.com/api'
};
